'use strict';

var RTM = require('satori-sdk-js');
var http = require('http'),
    request = require('request'),
    zlib = require('zlib'),
    inflate = require('./config/inflate.js'),
    config = require('./config/config.js'),
    fs = require('fs'),
    reqs = [];

var endpoint = "wss://open-data.api.satori.com";
var appkey = "appkeyHere";
var role = "aws-spot-instances-pricing";
var roleSecretKey = "roleSecretKeyHere";
var channel = "aws-spot-instances-pricing";

var roleSecretProvider = RTM.roleSecretAuthProvider(role, roleSecretKey);

var rtm = new RTM(endpoint, appkey, {
  authProvider: roleSecretProvider,
});

reqs[0] = config.awsprice;

var headers = {
  "accept-charset" : "ISO-8859-1,utf-8;q=0.7,*;q=0.3",
  "accept-language" : "en-US,en;q=0.8",
  "accept" : "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
  "user-agent" : "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/537.13+ (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
  "accept-encoding" : "gzip,deflate",
}
var options;

var subscription = rtm.subscribe(channel, RTM.SubscriptionMode.SIMPLE);

var compressedRequest = function(options, outStream) {
  var req = request(options, function(error, response, body){
    try {
      console.log('normalresponse', new Date().toLocaleString());
      var startPos = body.indexOf('({');
      var endPos = body.indexOf('})');
      var jsonString = body.substring(startPos+1, endPos+1);
      var jsondata = JSON.parse(jsonString);
      responseParse(jsondata);
    } catch (e) {
      console.log('parsing error' + e);
    }
  })

  req.on('response', function (res) {
    if (res.statusCode !== 200) throw new Error('Status not 200')
    var encoding = res.headers['content-encoding']
    if (encoding == 'gzip') {
      res.pipe(zlib.createGunzip())
      .setEncoding('utf8')
      .on('data', function(message){
          var test = message.replace(/[\n\r]+/g, '');
            try {
              console.log('gzipresponse', new Date().toLocaleString());
              var jsondata = JSON.parse(test);
              responseParse(jsondata);
            } catch (e) {
              console.log('parsing error' + e);
            }
      })
    } else if (encoding == 'deflate') {
      res.pipe(inflate.createInflate())
         .setEncoding('utf8')
         .on('data', function(message){
          var test = message.replace(/[\n\r]+/g, '');
            try {
              console.log('defateresponse', new Date().toLocaleString());
              var jsondata = JSON.parse(test);
              responseParse(jsondata);
            } catch (e) {
              console.log('parsing error' + e);
            }
        })
    } else {

    }
  })

  req.on('error', function(err) {
    throw err;
  })
}

var responseParse = function (jsondata){
  if (jsondata) {
    for (var i = 0; i < jsondata.config.regions.length; i++) {
      for (var j = 0; j < jsondata.config.regions[i].instanceTypes.length; j++) {
        for (var k = 0; k < jsondata.config.regions[i].instanceTypes[j].sizes.length; k++) {
          var message = {"formatVersion":"v1.0", "product":"EC2 Spot Instances"};
          message.region = jsondata.config.regions[i].region;
          message.instanceType = jsondata.config.regions[i].instanceTypes[j].instanceType;
          message.size = jsondata.config.regions[i].instanceTypes[j].sizes[k].size;
          message.price = jsondata.config.regions[i].instanceTypes[j].sizes[k].valueColumns[0].prices;
          publish(message);
        }
      }
    }
  }
}

var startStats = function (){
  console.log("startStats");
  for (var i = reqs.length - 1; i >= 0; i--) {
   options = {
     url : reqs[i],
     headers: headers
   };
    compressedRequest(options);
  } 
}

/* publish a message after being subscribed to sync on subscription */
subscription.on('enter-subscribed', function () {
  rtm.publish(channel, "Hello, World!", function (pdu) {
    //console.log("Publish ack:", pdu);
  });
  startStats();
});

/* set callback for PDU with specific action */
subscription.on('rtm/subscription/data', function (pdu) {
  pdu.body.messages.forEach(function (msg) {
    //console.log('Got message:', msg);
  });
  // close client after receving one message
  //rtm.stop();
});

var publish = function(message){
  rtm.publish(channel, message, function (pdu) {
    //console.log("Publish ack:", pdu);
  });
}

rtm.start();

setInterval(function(){
  startStats();
}, 1000 * 60);